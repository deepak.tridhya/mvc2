-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Feb 10, 2023 at 01:04 PM
-- Server version: 8.0.31
-- PHP Version: 8.0.26

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_php`
--

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
CREATE TABLE IF NOT EXISTS `user` (
  `id` int NOT NULL AUTO_INCREMENT,
  `image` varchar(50) NOT NULL,
  `firstname` varchar(50) NOT NULL,
  `lastname` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `gender` varchar(50) NOT NULL,
  `hobbies` varchar(50) NOT NULL,
  `designation` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=54 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `image`, `firstname`, `lastname`, `email`, `gender`, `hobbies`, `designation`, `password`) VALUES
(42, '6861featured3.png', 'Hilda', 'Ford', 'rekiwyc@mailinator.com', 'Male', 'Playing,Reading,Travelling,', 'trainee', 'Pa$$w0rd!'),
(41, '4372featured3.png', 'Mira', 'Beard', 'qonotexow@mailinator.com', 'Female', 'Playing,Travelling,', 'frontenddeveloper', 'Pa$$w0rd!'),
(40, '3809featured3.png', 'Mira', 'Beard', 'qonotexow@mailinator.com', 'Female', 'Playing,Travelling,', 'frontenddeveloper', 'Pa$$w0rd!'),
(39, '2237featured3.png', 'Nina', 'Hansen', 'nuzop@mailinator.com', 'Male', 'Playing,Reading,', 'trainee', 'Pa$$w0rd!'),
(38, '2202featured2.png', 'Colton', 'Lara', 'tezax@mailinator.com', 'Other', 'Playing,Travelling,', 'trainee', 'Pa$$w0rd!'),
(37, '8146featured2.png', 'Colton', 'Lara', 'tezax@mailinator.com', 'Other', 'Playing,Travelling,', 'trainee', 'Pa$$w0rd!'),
(36, '9159featured2.png', 'Colton', 'Lara', 'tezax@mailinator.com', 'Other', 'Playing,Travelling,', 'trainee', 'Pa$$w0rd!'),
(35, '9441featured1.png', 'Juliet', 'Schneider', 'cyved@mailinator.com', 'Other', 'Playing,Travelling,', 'frontenddeveloper', 'Pa$$w0rd!'),
(34, '4043featured1.png', 'Juliet', 'Schneider', 'cyved@mailinator.com', 'Other', 'Playing,Travelling,', 'frontenddeveloper', 'Pa$$w0rd!'),
(33, '7270Screenshot (1).png', 'Nehru', 'Jordan', 'lijarikad@mailinator.com', 'Female', 'Playing,Reading,', 'backenddeveloper', 'Pa$$w0rd!'),
(32, '8651Screenshot (1).png', 'Deepak', 'Nash', 'pyqudezy@mailinator.com', 'Other', 'Playing,Travelling,', 'frontenddeveloper', 'Pa$$w0rd!'),
(31, '2869Screenshot (1).png', 'Deepak', 'Nash', 'pyqudezy@mailinator.com', 'Other', 'Playing,Travelling,', 'frontenddeveloper', 'Pa$$w0rd!'),
(30, '5795Screenshot (1).png', 'Gillian', 'Mclaughlin', 'xigipafyt@mailinator.com', 'Male', 'Playing,', 'frontenddeveloper', 'Pa$$w0rd!'),
(29, '9587Screenshot (3).png', 'Pratush', 'Guy', 'figyn@mailinator.com', 'Female', 'Reading,', 'trainee', 'Pa$$w0rd!'),
(28, '5093Screenshot (3).png', 'Deepak Tripathi', 'xyz', 'deepakmegreat@gmail.com', 'Male', 'Playing,Reading,', 'frontenddeveloper', 'deepak@123'),
(27, '8304Screenshot (1).png', 'Mikayla', 'Aguirre', 'qeqadujyw@mailinator.com', 'Female', 'Playing,', 'frontenddeveloper', 'Pa$$w0rd!'),
(26, '3813Screenshot (1).png', 'Cleo', 'Lewis', 'fimykuh@mailinator.com', 'Male', 'Playing,Reading,Travelling,', 'backenddeveloper', 'Pa$$w0rd!'),
(43, '4211featured1.png', 'Jaquelyn', 'Hurley', 'widaco@mailinator.com', 'Female', 'Playing,Reading,Travelling,', 'frontenddeveloper', 'Pa$$w0rd!'),
(44, '1720featured1.png', 'Macey', 'Preston', 'hokise@mailinator.com', 'Other', 'Reading,', 'trainee', 'Pa$$w0rd!'),
(45, '2120featured1.png', 'Macy', 'Bender', 'luzupohot@mailinator.com', 'Other', 'Playing,Reading,', 'trainee', 'Pa$$w0rd!'),
(46, '2170featured1.png', 'DeepakWinter', 'Bonner', 'vocywyju@mailinator.com', 'Other', 'Reading,Travelling,', 'frontenddeveloper', 'Pa$$w0rd!'),
(47, '3457featured1.png', 'Mallory', 'Burt', 'pibavejuw@mailinator.com', 'Male', 'Playing,Reading,', 'trainee', 'Pa$$w0rd!'),
(48, '8404featured1.png', 'Mallory', 'Burt', 'pibavejuw@mailinator.com', 'Male', 'Playing,Reading,', 'trainee', 'Pa$$w0rd!'),
(49, '3235featured1.png', 'Travis', 'Holman', 'kasu@mailinator.com', 'Other', 'Reading,Travelling,', 'backenddeveloper', 'Pa$$w0rd!'),
(50, '2642featured1.png', 'Travis', 'Holman', 'kasu@mailinator.com', 'Other', 'Reading,Travelling,', 'backenddeveloper', 'Pa$$w0rd!'),
(51, '9827featured1.png', 'Travis', 'Holman', 'kasu@mailinator.com', 'Other', 'Reading,Travelling,', 'backenddeveloper', 'Pa$$w0rd!'),
(52, '7040featured1.png', 'Travis', 'Holman', 'kasu@mailinator.com', 'Other', 'Reading,Travelling,', 'backenddeveloper', 'Pa$$w0rd!'),
(53, '9219featured1.png', 'Clementine', 'Dickson', 'zohoj@mailinator.com', 'Male', 'Travelling,', 'frontenddeveloper', 'Pa$$w0rd!');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
