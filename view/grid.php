
<html>
<head>
 <title>PHP Assignment</title>
 <!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">  

</head>        
<body>
<div class="container">
    <div class="jumbotron text-center">
        <h2>PHP Assignment</h2>
    </div>
    <br>
    <a href="index.php?controller=EmployeesController&action=login_view" role="button" class="btn btn-primary pull-right">Logout</a> 
    <br>
    <br>
    <table class="table table-hover table-striped">
        <tr>
            <th>Sr</th>
            <th>Name</th>
            <th>Email</th>
            <th>Gender</th>
            <th>Hobbies</th>
            <th>Designation</th>
            <!-- <th>Mobile</th> -->
            <th>Image</th>
            <th>Action</th>
        </tr>
        <?php
       
        while($row = mysqli_fetch_assoc($result))
        {
           
            echo "<tr>";
            echo "<td>". $row['id'] ."</td>";
            echo "<td>". $row['firstname']. ' ' .$row['lastname'] ."</td>";
            echo "<td>". $row['email'] ."</td>";
            echo "<td>". $row['gender'] ."</td>";
            echo "<td>". $row['hobbies'] ."</td>";
            echo "<td>". $row['designation'] ."</td>";
           // echo "<img src="data:image/jpg;charset=utf8;base64,<?php echo base64_encode($row['image']); 
           echo "<td>";
           echo "<img src='image/" .$row['image'] . "' alt='Image' width='100px'>";
           echo "</td>";
           echo "<td>
           <a href='index.php?controller=EmployeesController&action=update_view&id=" . $row['id'] . "'>Edit</a>
           <a href='index.php?controller=EmployeesController&action=delete&id=" . $row['id'] . "'>Delete</a>
                </td>";
         echo "</tr>";
       

          }
          ?>
    </table>
</div>

</body>

<!-- jQuery library -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

<!-- Latest compiled JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script src="https://kit.fontawesome.com/yourcode.js" crossorigin="anonymous"></script> 
</html>