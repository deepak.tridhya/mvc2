<html>
<head>
   <title>Update Page</title>  
   <!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<head>
<body>
 
<div class="container">

<div class="jumbotron text-center">
    <h2>Update Your Information</h2>
</div>
<br>
<div class="row">
<div class="col-md-12">

<form action="index.php?controller=EmployeesController&action=update&id=<?php echo $row["id"]?>" method="post" enctype="multipart/form-data">
<div class="form-group">
    <label for="image">Image:</label>
    <img src= "<?= "image/".$row['image']?>" alt="" width="100px" height="100px" class="thumbnail">
    <input type="file" class="btn btn-primary" name="image" id="image" class="form-control" >
</div>

<div class="form-group">
    <label for="firstname">First Name:</label>
    <input type="text" name="firstname" id="firstname" class="form-control" placeholder="first name" value="<?php echo $row['firstname']; ?>">
</div>
<div class="form-group">
    <label for="lastname">Last Name:</label>
    <input type="text" name="lastname" id="lastname" class="form-control" placeholder="last name" value="<?php echo $row['lastname']; ?>" >
</div>
<div class="form-group">
    <label for="email">Email</label>
    <input id="email"  name="email" type="email" class="form-control" placeholder="Email" value="<?php echo $row['email']; ?>" >
</div>

<div class="form-group">
  <label for="gender">Gender:</label><br>
 <input type="radio" id="male" name="gender" <?php if($row['gender']=="Male"){echo "checked";}?>   value="Male" >
 <label for="male">Male</label><br>

 <input type="radio"  id="female" name="gender"  <?php if($row['gender']=="Female"){echo "checked";}?>   value="Female" >
 <label for="female">Female</label><br>

 <input type="radio" id="other" name="gender"  <?php if($row['gender']=="Other"){echo "checked";}?>   value="Other" >
 <label for="other">Other</label>
</div>

<div class="form-group">
    <label for="hobbies">Hobbies:</label><br>
    <input type="checkbox"  name="hobbies[]" value="Playing" <?php if($hobbies[0]=="Playing"||$hobbies[1]=="Playing"||$hobbies[2]=="Playing"){echo 'checked="checked"';} ?>>
    <label >Playing</label><br>
    <input type="checkbox"  name="hobbies[]" value="Reading" <?php if($hobbies[0]=="Reading"||$hobbies[1]=="Reading"||$hobbies[2]=="Reading"){echo 'checked="checked"';} ?>>
    <label >Reading</label><br>
    <input type="checkbox"  name="hobbies[]" value="Travelling" <?php if($hobbies[0]=="Travelling"||$hobbies[1]=="Travelling"||$hobbies[2]=="Travelling"){echo 'checked="checked"';} ?>>
    <label for="vehicle3">Travelling</label><br>
</div>
<div class="form-group">
    <label for="designation">Designation:</label><br>

    <select name="designation" id="designation" class="form-control" value="<?// echo $row['designation']; ?>">

    
    <option value="trainee" <?php if($row['designation']=="trainee") {echo 'selected="selected"';} ?> >Trainee</option>
    <option value="frontenddeveloper" <?php if($row['designation']=="frontenddeveloper") {echo 'selected="selected"';} ?>>Front End Developer</option>
    <option value="backenddeveloper" <?php if($row['designation']=="backenddeveloper") {echo 'selected="selected"'; }?>>Back End Developer</option>
    
  </select>
</div>



<div class="form-group">
    <input type="submit" class="btn btn-success" value="Update" name="update">
</div>

</form>
</div>
</div>

</div>
<body>
   <!-- jQuery library -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

<!-- Latest compiled JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</html>
