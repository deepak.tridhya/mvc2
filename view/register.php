<!DOCTYPE html>
<html>
<head>
  <title>Insert Data in MySQL using PHP MVC</title>
  <!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
</head>

<body>
<!-- 
<form action="index.php?controller=EmployeesController&action=save" method="post" enctype="multipart/form-data">
<p>
   <label>Image</label>
   <input id="image" value="" name="image" type="file"  /><br />
  </p>
  <p>
   <label>First Name</label>
   <input id="firstname" value="" name="firstname" type="text" required="required" /><br />
  </p>
   <p> 
  <label>Last Name</label>
   <input id="lastname" value="" name="lastname" type="text" required="required" /><br />
  </p>
   <p>   
  <label>Email</label>
   <input id="email" value="" name="email" type="email" required="required" /><br />
  </p>

  <p>
  <p>Gender:</p>
    <input type="radio" id="male" name="gender" value="Male">
     <label for="male">Male</label><br>
      <input type="radio"  id="female" name="gender" value="Female">
       <label for="female">Female</label><br>
        <input type="radio" id="other" name="gender" value="Other">
         <label for="other">Other</label>
  </p>

  
  <p>

  <p>Hobbies:</p>
<input type="checkbox"  name="hobbies[]" value="Playing">
<label >Playing</label><br>
<input type="checkbox"  name="hobbies[]" value="Reading">
<label >Reading</label><br>
<input type="checkbox"  name="hobbies[]" value="Travelling">
<label for="vehicle3">Travelling</label><br>
  </p>
  <p>
<label for="degignation">Choose Designation:</label>

<select name="designation" id="designation">
  <option value="trainee">Trainee</option>
  <option value="frontenddeveloper">Front End Developer</option>
  <option value="backenddeveloper">Back End Developer</option>
  
</select>

</p>


   <p>
   <label>Password</label>
   <input id="password" value="" name="password" type="password" required="required" /><br>
  </p>
  <p>
   <label>Check Password</label>
   <input id="cpassword" value="" name="cpassword" type="cpassword" required="crequired" /><br>
  </p>

   <br />

  <p>

      <button type="submit"  id="submit" name="submit" ><span>Register</span></button><p>
      <p>IF YOU ALREADY HAVE AN ACCOUNT</p> 
      <a href="view/login.php">
        <button>Login</button>
      </a>
  </p>
 </form> -->

<!-- Other
Other -->



<div class="container">

    <div class="jumbotron text-center">
        <h2>Register to create account</h2>
    </div>
    <br>
<div class="row">
<div class="col-md-12">
    
<form action="index.php?controller=EmployeesController&action=save" method="post" enctype="multipart/form-data">
    <div class="form-group">
        <label for="image">Image:</label>
        <input type="file" class="btn btn-primary" name="image" id="image" class="form-control" value="">
    </div>
   
    <div class="form-group">
        <label for="firstname">First Name:</label>
        <input type="text" name="firstname" id="firstname" class="form-control" placeholder="first name" value="">
    </div>
    <div class="form-group">
        <label for="lastname">Last Name:</label>
        <input type="text" name="lastname" id="lastname" class="form-control" placeholder="last name" value="">
    </div>
    <div class="form-group">
        <label for="email">Email</label>
        <input id="email" value="" name="email" type="email" class="form-control" placeholder="Email" >
    </div>
    <div class="form-group">
      <label for="gender">Gender:</label><br>
     <input type="radio" id="male" name="gender"  value="Male">
     <label for="male">Male</label><br>
      <input type="radio"  id="female" name="gender" value="Female">
       <label for="female">Female</label><br>
        <input type="radio" id="other" name="gender"  value="Other">
         <label for="other">Other</label>
    </div>

    <div class="form-group">
        <label for="hobbies">Hobbies:</label><br>
        <input type="checkbox"  name="hobbies[]" value="Playing">
        <label >Playing</label><br>
        <input type="checkbox"  name="hobbies[]" value="Reading">
        <label >Reading</label><br>
        <input type="checkbox"  name="hobbies[]" value="Travelling">
        <label for="Travelling">Travelling</label><br>
    </div>
    <div class="form-group">
        <label for="designation">Designation:</label><br>
        <select name="designation" id="designation" class="form-control">
        <option value="0">Select your designation....</option>  
        <option value="trainee">Trainee</option>
        <option value="frontenddeveloper">Front End Developer</option>
        <option value="backenddeveloper">Back End Developer</option>
        
      </select>
    </div>
    
    <div class="form-group">
        <label for="name">Password:</label>
        <input id="password" value="" name="password" type="password"  class="form-control"/><br>
    </div>
    <div class="form-group">
        <label for="name">Check Password:</label>
        <input id="cpassword" value="" name="cpassword" type="password"  class="form-control"/><br>
    </div>
   
    <div class="form-group">
        <input type="submit" class="btn btn-success" value="Register" name="register">
    </div>
    <p>Already have an account? <a href="index.php?controller=EmployeesController&action=login_view">Login here</a>.</p>
</form>
</div>
</div>

</div>


<!-- jQuery library -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

<!-- Latest compiled JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>







</body>
</html>