<?php
class EmployeesController
{
    private $conectar;
    private $Connection;
    public $model;
    public function __construct()
    {
        require_once "model/employee.php";
        $this->model = new Employee();
    }
    /**
     * Ejecuta la acción correspondiente.
     *
     */
    public function run($accion)
    {
        switch ($accion) {
            case "index":
                $this->index();
                break;
            case "save":
                $this->save();
                break;
            case "show":
                $this->show();
                break;
            case "login_view":
                $this->login_view();
                break;
            case "login":
                $this->login();
                break;
            case "update_view":
                $this->update_view();
                break;
            case "update":
                    $this->update();
                    break;    
            case "delete":
                $this->delete();
                break;
            default:
                $this->index();
                break;
        }
    }

    /**
     * Loads the employees home page with the list of
     * employees getting from the model.
     *
     */
    public function index()
    {
        include "view/register.php";
    }
   
    public function save()
    {
        $result = $this->model->insert_data();
        $this->login_view();
    }

    public function show()
    {
        $result = $this->model->view_data();
        include "view/grid.php";
    }

    public function login_view()
    {
        include "view/login.php";
    }

    public function login()
    {
        $result = $this->model->login_check();

        if ($result == 1) {
            $this->show();
        } else {
            echo "User Does Not Exits Plese Signup";
            $this->index();
        }
    }

    public function delete()
    {
        $this->model->delete_data();
        $this->show();
    }

    public function update_view()
    {
        $result = $this->model->update_data_view();
        $row = mysqli_fetch_assoc($result);
        $hobbies= explode(',', $row['hobbies']);
        include "view/update.php";

    }
    
    public function update(){
        $this->model->update_data();
        $this->show();
    }
}
?>
